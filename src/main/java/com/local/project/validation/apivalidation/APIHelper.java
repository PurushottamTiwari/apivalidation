package com.local.project.validation.apivalidation;

import io.jsonwebtoken.Claims;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class APIHelper {
    private  List<String> values = new ArrayList<String>();


    public List<String> getValues() {
        return values;
    }



    public  Date getExpirationTime( Calendar calendar) {

        calendar.add(Calendar.SECOND, 60);
        return calendar.getTime();
    }

    public  boolean checkValues(Claims claims, Token token) {

        if (!((String) claims.get(APIConstants.REFERENCE_VALUE)).equalsIgnoreCase(token.getServiceIdentifierReference()))
            return false;
        return ((String) claims.get(APIConstants.REFERENCE_TYPE)).equalsIgnoreCase(token.getServiceIdentifierReferenceType());
    }
}
