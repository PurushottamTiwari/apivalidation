package com.local.project.validation.apivalidation;

import lombok.Getter;
import lombok.Setter;


public class Response {

    protected String apiVersion;
    protected String requestionBody;
    protected String response;
    protected String failureReason;
    private String token;

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getRequestionBody() {
        return requestionBody;
    }

    public void setRequestionBody(String requestionBody) {
        this.requestionBody = requestionBody;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
