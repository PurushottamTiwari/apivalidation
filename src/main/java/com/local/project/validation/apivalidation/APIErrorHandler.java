package com.local.project.validation.apivalidation;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Component
public class APIErrorHandler {

    @Autowired
    APIHelper apiHelper;


    @ExceptionHandler(ExpiredJwtException.class)
    @ResponseBody
    public Response handleError(ExpiredJwtException e) {
        Response response = new Response();
        response.setApiVersion(APIConstants.API_VERSION);
        response.setRequestionBody(apiHelper.getValues().get(0));
        response.setResponse("FAIL");
        response.setFailureReason("Token Expired");
        return response;
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Response handleError2(Exception e) {
        Response response = new Response();
        response.setApiVersion(APIConstants.API_VERSION);
        response.setRequestionBody(apiHelper.getValues().get(0));
        response.setResponse("FAIL");
        response.setFailureReason(e.getMessage());
        return response;
    }
}
