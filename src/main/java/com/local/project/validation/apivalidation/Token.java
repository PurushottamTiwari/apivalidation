package com.local.project.validation.apivalidation;

import lombok.Getter;
import lombok.Setter;


public class Token {
    private String issuer;
    private String auidence;
    private String subject;
    private String securityKey;
    private String serviceIdentifierReference;
    private String serviceIdentifierReferenceType;
    private String token;

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getAuidence() {
        return auidence;
    }

    public void setAuidence(String auidence) {
        this.auidence = auidence;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }

    public String getServiceIdentifierReference() {
        return serviceIdentifierReference;
    }

    public void setServiceIdentifierReference(String serviceIdentifierReference) {
        this.serviceIdentifierReference = serviceIdentifierReference;
    }

    public String getServiceIdentifierReferenceType() {
        return serviceIdentifierReferenceType;
    }

    public void setServiceIdentifierReferenceType(String serviceIdentifierReferenceType) {
        this.serviceIdentifierReferenceType = serviceIdentifierReferenceType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
