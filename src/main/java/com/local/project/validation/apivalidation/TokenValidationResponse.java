package com.local.project.validation.apivalidation;


public class TokenValidationResponse extends Response {

    private String apiVersion;
    private String requestionBody;
    private String response;

    @Override
    public String getApiVersion() {
        return apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public String getRequestionBody() {
        return requestionBody;
    }

    @Override
    public void setRequestionBody(String requestionBody) {
        this.requestionBody = requestionBody;
    }

    @Override
    public String getResponse() {
        return response;
    }

    @Override
    public void setResponse(String response) {
        this.response = response;
    }
}
