package com.local.project.validation.apivalidation;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.DatatypeConverter;
import java.util.Calendar;


@RestController
public class ValidationController extends  APIErrorHandler{


    @Autowired
    APIHelper apiHelper;


    @RequestMapping(path = APIConstants.GENERATE_TOKEN_SERVICE_PATH, method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Response generateToken(@RequestBody Token token) throws Exception {
        Calendar calendar = Calendar.getInstance();
        apiHelper.getValues().add(token.getAuidence());

        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setIssuer(token.getIssuer());
        jwtBuilder.setAudience(token.getAuidence());
        jwtBuilder.setSubject(token.getSubject());
        jwtBuilder.setIssuedAt(calendar.getTime());
        jwtBuilder.setExpiration(apiHelper.getExpirationTime(calendar));

        Claims claims = Jwts.claims();
        claims.put(APIConstants.REFERENCE_TYPE, token.getServiceIdentifierReferenceType());
        claims.put(APIConstants.REFERENCE_VALUE, token.getServiceIdentifierReference());

        jwtBuilder.addClaims(claims);
        byte[] keys = DatatypeConverter.parseBase64Binary(token.getSecurityKey());
        jwtBuilder.signWith(SignatureAlgorithm.HS256, keys);
        String tokenVal = jwtBuilder.compact();

        Response tokenGenerationResponse = new Response();
        tokenGenerationResponse.setApiVersion(APIConstants.API_VERSION);
        tokenGenerationResponse.setRequestionBody(token.getAuidence());
        tokenGenerationResponse.setResponse("SUCCESS");
        tokenGenerationResponse.setToken(tokenVal);
        return tokenGenerationResponse;

    }

    @RequestMapping(path = APIConstants.VALIDATE_TOKEN_SERVICE_PATH, method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public TokenValidationResponse verifyToken(@RequestBody Token token) throws Exception {
        apiHelper.getValues().add(token.getAuidence());
        TokenValidationResponse tokenValidationResponse = new TokenValidationResponse();
        tokenValidationResponse.setApiVersion(APIConstants.API_VERSION);
        tokenValidationResponse.setRequestionBody(token.getAuidence());
        byte[] keys = DatatypeConverter.parseBase64Binary(token.getSecurityKey());
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(keys).parseClaimsJws(token.getToken());

        Claims claims = claimsJws.getBody();

        if (apiHelper.checkValues(claims, token)) {
            tokenValidationResponse.setResponse("SUCCESS");
        } else {
            tokenValidationResponse.setResponse("FAIL");
            tokenValidationResponse.setFailureReason("Invalid Token");
        }
        return tokenValidationResponse;
    }

}

