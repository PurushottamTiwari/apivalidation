package com.local.project.validation.apivalidation;

import io.jsonwebtoken.Claims;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class APIConstants {

    public static final String GENERATE_TOKEN_SERVICE_PATH = "/generatetoken";
    public static final String VALIDATE_TOKEN_SERVICE_PATH = "/verifytoken";
    public static final String REFERENCE_TYPE = "SERVICE_IDENTIFIER_REFERENCE_TYPE";
    public static final String REFERENCE_VALUE = "SERVICE_IDENTIFIER_REFERENCE";
    public static final String SECURITY_KEY = "SECURITY_KEY";
    public static final String API_VERSION = "TOKEN API 1.0";
    public static final String GENERATE_TOKEN_URL = "http://localhost:9095/generatetoken";
    public static final String VERIFY_TOKEN_URL = "http://localhost:9095/verifytoken";





}
