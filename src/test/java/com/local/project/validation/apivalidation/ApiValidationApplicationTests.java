package com.local.project.validation.apivalidation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ApiValidationApplicationTests {

    MockMvc mockMvc;
    MvcResult mvcResult;
    @Autowired
    WebApplicationContext webApplicationContext;
    private final Logger logger = LoggerFactory.getLogger(ApiValidationApplicationTests.class);

    @Test
    public void testPositiveCase() {
        try {
            mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
            //Generate Token
            String requestToken = getToken(mockMvc);
            assert (!StringUtils.isEmpty(requestToken));
            //VerifyToken
            Response response = verifyToken(requestToken , mockMvc);
            assert (response != null &&  "Success".equalsIgnoreCase(response.getResponse()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    @Test
    public void testTokenExpiredErrorMessage(){
        try {
            mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
            //Generate Token
            String requestToken = getToken(mockMvc);
            assert (!StringUtils.isEmpty(requestToken));
            Thread.sleep(65000);
            //VerifyToken
            Response response = verifyToken(requestToken , mockMvc);
            assert (response != null && "FAIL".equalsIgnoreCase(response.getResponse()) && "Token Expired".equalsIgnoreCase(response.getFailureReason()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    public void testGeneralExceptionErrorMessage(){
        try {
            mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
            //Generate Token
            String requestToken = getToken(mockMvc);
            assert (!StringUtils.isEmpty(requestToken));
            //VerifyToken
            Response response = verifyToken("" , mockMvc);
            assert (response != null && "FAIL".equalsIgnoreCase(response.getResponse()) && "JWT String argument cannot be null or empty.".equalsIgnoreCase(response.getFailureReason()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    private String getToken(MockMvc mockMvc )throws Exception{
        Token token = getTokenObject();
        String requestStr = getRequestJsonString(token);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.request(HttpMethod.POST, APIConstants.GENERATE_TOKEN_URL).contentType("application/json").content(requestStr);
        mvcResult = mockMvc.perform(requestBuilder).andReturn();
        String responseStr = mvcResult.getResponse().getContentAsString();
        Response response = getResponseObjectFromResponseString(responseStr);
        String requestToken = response.getToken();
        return requestToken;
    }

    private Response verifyToken(String requestToken,MockMvc mockMvc)throws Exception{
        Token token = getTokenObject();
        token.setToken(requestToken);
        String requestStr = getRequestJsonString(token);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.request(HttpMethod.POST, APIConstants.VERIFY_TOKEN_URL).contentType("application/json").content(requestStr);
        mvcResult = this.mockMvc.perform(requestBuilder).andReturn();
        String responseStr = mvcResult.getResponse().getContentAsString();
        Response response = getResponseObjectFromResponseString(responseStr);
        return response;
    }

    private Token  getTokenObject(){
        Token token = new Token();
        token.setAuidence("Purushottam-hp");
        token.setIssuer("PurushottamTiwari");
        token.setSubject("API");
        token.setSecurityKey("india#2013");
        token.setServiceIdentifierReferenceType("servicerequestno");
        token.setServiceIdentifierReference("123456");
        return token;
    }

    private String getRequestJsonString(Token token) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String jsonText = objectWriter.writeValueAsString(token);
        return jsonText;
    }

    private Response  getResponseObjectFromResponseString(String response) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectReader objectReader = objectMapper.reader();
        Response serviceResponse = objectReader.readValue(response, Response.class);
        return serviceResponse;
    }

}
